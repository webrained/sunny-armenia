$(document).ready(function(){
    $('#carousel').on('slid.bs.carousel', function (e) {
        var active =$(this).find('.active').index();
        var to = $(e.relatedTarget).index();

        $('#thumbcarousel').find('.showing').removeClass('showing');
        $('#thumbcarousel').find('[data-slide-to='+active+']').addClass('showing');

    });

    $('#carousel').on('slide.bs.carousel', function (e) {
        var active =$(this).find('.active').index();
        var to = $(e.relatedTarget).index();

        if(active == 4 && to == 5) {
            $('#thumbcarousel').carousel('next');
        }
        if(active == 9 && to == 0) {
            $('#thumbcarousel').carousel('next');
        }
    });

    $('.carousel-control-prev-icon').css('background-image', 'url("data:image/svg+xml,%3csvg xmlns=\'http://www.w3.org/2000/svg\' fill=\'\' viewBox=\'0 0 8 8\'%3e%3cpath d=\'M5.25 0l-4 4 4 4 1.5-1.5-2.5-2.5 2.5-2.5-1.5-1.5z\'/%3e%3c/svg%3e")');
    $('.carousel-control-next-icon').css('background-image', 'url("data:image/svg+xml,%3csvg xmlns=\'http://www.w3.org/2000/svg\' fill=\'\' viewBox=\'0 0 8 8\'%3e%3cpath d=\'M2.75 0l-1.5 1.5 2.5 2.5-2.5 2.5 1.5 1.5 4-4-4-4z\'/%3e%3c/svg%3e")')

});

$(".staff").on("click", function(){
        $(".staff").removeClass("expand unset ");
        $(".staff").addClass("small");
        $(this).removeClass("small");
        $(this).addClass("expand");
    }
);

